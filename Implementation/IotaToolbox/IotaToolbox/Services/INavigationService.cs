﻿using EasyConsole;

namespace Com.TomRiedl.IotaToolbox.Services
{
    public interface INavigationService
    {
        void Initialize();

        void Run();

        void NavigateHome();

        TPage NavigateTo<TPage>() where TPage : Page;
    }
}