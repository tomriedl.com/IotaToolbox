﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Com.TomRiedl.IotaToolbox.Core;
using Com.TomRiedl.IotaToolbox.Pages;
using EasyConsole;

namespace Com.TomRiedl.IotaToolbox.Services.Implementation
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class NavigationService: Program, INavigationService
    {
        public NavigationService()
            : base(AppConstants.AppName, true)
        {
        }

        public void Initialize()
        {
            AddPages();

            SetPage<MainPage>();
        }

        private void AddPages()
        {
            var pages = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(type => typeof(MenuPage).IsAssignableFrom(type));

            foreach (var page in pages)
            {
                var instance = Ioc.Construct(page) as MenuPage;
                AddPage(instance);
            }
        }
    }
}
