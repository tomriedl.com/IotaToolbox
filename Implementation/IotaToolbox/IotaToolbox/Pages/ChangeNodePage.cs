﻿using System;
using System.Diagnostics.CodeAnalysis;
using Com.TomRiedl.IotaToolbox.Properties;
using Com.TomRiedl.IotaToolbox.Services;
using EasyConsole;

namespace Com.TomRiedl.IotaToolbox.Pages
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    internal class ChangeNodePage: MenuPage
    {
        private readonly INavigationService _navigationService;
        public static string DisplayTitle { get;  } = "Change Node";

        public ChangeNodePage(INavigationService navigationService)
            : base(DisplayTitle, navigationService as Program)
        {
            _navigationService = navigationService;
        }

        public override void Display()
        {
            Output.WriteLine($"Current node: {Settings.Default.NodeProtocol}://{Settings.Default.NodeHost}:{Settings.Default.NodePort}/");

            Settings.Default.NodeProtocol = Input.ReadString("Protocol (e.g. http):").Trim();
            Settings.Default.NodeProtocol = string.IsNullOrEmpty(Settings.Default.NodeProtocol)
                ? "http"
                : Settings.Default.NodeProtocol;
            Settings.Default.NodeHost = Input.ReadString("Host (e.g. iota.bitfinex.com):");
            Settings.Default.NodeHost = string.IsNullOrEmpty(Settings.Default.NodeHost)
                ? "iota.bitfinex.com"
                : Settings.Default.NodeHost;
            Settings.Default.NodePort = Input.ReadInt("Port (e.g. 80):", 0, ushort.MaxValue);
            Settings.Default.NodePort = Settings.Default.NodePort <= 0
                ? 80
                : Settings.Default.NodePort;

            Settings.Default.Save();

            Output.WriteLine($"{Environment.NewLine}{Environment.NewLine}Press any key to go back to the main menu.");
            Console.ReadKey();
            _navigationService.NavigateHome();
        }
    }
}
