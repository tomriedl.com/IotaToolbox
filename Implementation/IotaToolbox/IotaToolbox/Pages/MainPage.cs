﻿using System;
using System.Diagnostics.CodeAnalysis;
using Com.TomRiedl.IotaToolbox.Core;
using Com.TomRiedl.IotaToolbox.Properties;
using Com.TomRiedl.IotaToolbox.Services;
using EasyConsole;

namespace Com.TomRiedl.IotaToolbox.Pages
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class MainPage : MenuPage
    {
        private readonly Option _nodeMenuOption;

        public MainPage(INavigationService navigationService)
            : base($"{AppConstants.AppName} - {AppConstants.AppDomain}", navigationService as Program)
        {
            _nodeMenuOption = new Option(GetNodeMenuTitle(), () => navigationService.NavigateTo<ChangeNodePage>());

            Menu.Add(new Option(FindBalancePage.DisplayTitle, () => navigationService.NavigateTo<FindBalancePage>()));
            Menu.Add(_nodeMenuOption);
            Menu.Add(new Option("Exit", () => Environment.Exit(0)));
        }

        private string GetNodeMenuTitle()
        {
            return $"{ChangeNodePage.DisplayTitle} (Current: {Settings.Default.NodeProtocol}://{Settings.Default.NodeHost}:{Settings.Default.NodePort}/)";
        }

        public override void Display()
        {
            // ReSharper disable once PossibleNullReferenceException
            _nodeMenuOption.GetType().GetProperty("Name").SetValue(_nodeMenuOption, GetNodeMenuTitle(), null);

            base.Display();
        }
    }
}
