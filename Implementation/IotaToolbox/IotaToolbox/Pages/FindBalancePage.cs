﻿using System;
using System.Diagnostics.CodeAnalysis;
using Com.TomRiedl.IotaToolbox.Services;
using EasyConsole;

namespace Com.TomRiedl.IotaToolbox.Pages
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class FindBalancePage: MenuPage
    {
        private readonly INavigationService _navigationService;

        public static string DisplayTitle { get; } = "Find Balance";

        public FindBalancePage(INavigationService navigationService)
            : base(DisplayTitle, navigationService as Program)
        {
            _navigationService = navigationService;
        }

        public override void Display()
        {
            var seed = Input.ReadString("Please enter your seed:").Trim();
            Output.WriteLine("Checking balance for seed " + seed);

            Output.WriteLine($"{Environment.NewLine}{Environment.NewLine}Press any key to go back to the main menu.");
            Console.ReadKey();
            _navigationService.NavigateHome();
        }
    }
}
