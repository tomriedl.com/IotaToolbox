﻿using Com.TomRiedl.IotaToolbox.Core;
using Com.TomRiedl.IotaToolbox.Services;
using Com.TomRiedl.IotaToolbox.Services.Implementation;

namespace Com.TomRiedl.IotaToolbox
{
    public static class EntryPoint
    {
        static int Main(string[] args)
        {
            ServiceRegistration.Register();

            var navigationService = Ioc.Resolve<INavigationService>();
            navigationService.Initialize();
            navigationService.Run();

            return 0;
        }
    }
}
