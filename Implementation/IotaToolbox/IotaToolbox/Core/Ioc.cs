using System;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace Com.TomRiedl.IotaToolbox.Core
{
    public static class Ioc
    {
        public static WindsorContainer Container { get; } = new WindsorContainer();

        public static void RegisterImplementation<TInterface, TImplementation>()
            where TInterface : class
            where TImplementation : TInterface
        {
            RegisterImplementation<TInterface>(typeof(TImplementation));
        }

        public static void RegisterImplementation<TInterface>(Type implementation)
            where TInterface : class
        {
            Container.Register(Component.For<TInterface>().ImplementedBy(implementation));
        }

        public static void RegisterInstance<TInterface>(TInterface instance)
            where TInterface : class
        {
            Container.Register(Component.For<TInterface>().Instance(instance));
        }

        public static TInterface Resolve<TInterface>()
            where TInterface : class
        {
            return Container.Resolve<TInterface>();
        }

        public static TType Construct<TType>()
            where TType : class
        {
            var type = typeof(TType);
            if (!Container.Kernel.HasComponent(type))
            {
                Container.Register(Component.For<TType>().Named(type.FullName).LifeStyle.Is(LifestyleType.Transient));
            }

            return Container.Resolve(type) as TType;
        }

        public static object Construct(Type type)
        {
            if (!Container.Kernel.HasComponent(type))
            {
                Container.Register(Component.For(type).Named(type.FullName).LifeStyle.Is(LifestyleType.Transient));
            }

            return Container.Resolve(type);
        }
    }
}