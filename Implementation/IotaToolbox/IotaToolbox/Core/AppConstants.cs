﻿namespace Com.TomRiedl.IotaToolbox.Core
{
    public static class AppConstants
    {
        public static string AppName { get; } = "IOTA Toolbox";
        public static string AppDomain { get; } = "tomriedl.com";
    }
}
