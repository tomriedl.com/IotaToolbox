﻿using Com.TomRiedl.IotaToolbox.Services;
using Com.TomRiedl.IotaToolbox.Services.Implementation;

namespace Com.TomRiedl.IotaToolbox.Core
{
    internal static class ServiceRegistration
    {
        public static void Register()
        {
            Ioc.RegisterImplementation<INavigationService, NavigationService>();
        }
    }
}
